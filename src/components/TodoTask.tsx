import React from "react";
/** @jsxRuntime classic */
/** @jsx jsx */
import { jsx, css } from "@emotion/react";
import { store } from "../store/TodoStore";
import { getSnapshot } from "mobx-state-tree";
import { observer } from "mobx-react";
import ListTask from "./ListTask";
import Swal from "sweetalert2";
import UseAddTask from "./hooks/UseAddTask";

const container = css`
  display: flex;
  justify-content: center;
  flex-direction: column;
  padding: 0;
  margin: 0;
  list-style: none;
  text-align: center;
`;

const title = css`
  font-size: 50px;
  font-weight: bold;
`;

const containerTaskDone = css`
  margin-top: 5rem;
`;

const containerInputTask = css`
  margin-top: 3rem;
`;

const inputTask = css`
  width: 250px;
  height: 40px;
  font-size: 20px;
  border: 1px solid #bebebe;
  border-radius: 8px;
`;

const buttonInpuTask = css`
  width: 100px;
  height: 40px;
  font-size: 20px;
  border: 1px solid #bebebe;
  border-radius: 8px;
  margin-left: 1.5rem;
  font-weight: lighter;
  margin-bottom: 2rem;
  cursor: pointer;
`;

const buttonTaskDone = css`
  width: 250px;
  height: 50px;
  font-size: 20px;
  border: 1px solid #bebebe;
  border-radius: 8px;
  font-weight: lighter;
  cursor: pointer;
`;

const TodoTask: React.FC = () => {
  let indexStorage: number = store.todos.length;

  const [valueTask, setValueTask] = React.useState<{
    task: string;
    done: boolean;
  }>({ task: "", done: false });

  const [valueTaskAux, setValueTaskAux] = React.useState<{
    task: string;
  }>({ task: "" });

  const [checkSendTask, setCheckSendTask] = React.useState<boolean>(true);

  const [hideShowTaskDone, setHideShowTaskDone] = React.useState<boolean>(true);

  const [valueTaskDone, setValueTaskDone] = React.useState<
    {
      task: string;
      done: boolean;
    }[]
  >([]);

  React.useEffect(() => {
    if (valueTask.task.length === 0) {
      setCheckSendTask(true);
    } else {
      setCheckSendTask(false);
    }
  }, [valueTask, checkSendTask]);

  React.useEffect(() => {
    store.todos.forEach((e) => {
      if (e.name === valueTaskAux.task) {
        e.toggle();
      }
    });

    let taskIncomplete = store.todos.filter((e: any) => e.done === true);
    if (taskIncomplete.length > 0) {
      setHideShowTaskDone(true);
    } else {
      setHideShowTaskDone(false);
    }
  }, [valueTaskAux]);

  React.useEffect(() => {
    localStorage.setItem("TasksHidens", JSON.stringify(valueTaskDone));
  }, [hideShowTaskDone]);

  const handleOnChange = (e: React.ChangeEvent<HTMLInputElement>) => {
    const taskToCreate = e.target.value;
    setValueTask((prevState) => ({
      ...prevState,
      task: taskToCreate,
    }));
  };

  const sendTask = (e: React.MouseEvent<HTMLElement>) => {
    UseAddTask(e, valueTask, indexStorage, setValueTask);
  };

  const handleKeyPress = (e: React.KeyboardEvent<HTMLDivElement>) => {
    UseAddTask(e, valueTask, indexStorage, setValueTask);
  };

  const handleCheck = (e: React.ChangeEvent<HTMLInputElement>) => {
    setValueTaskAux({ task: e.target.value });
  };

  const ShowTaskDone = () => {
    if (hideShowTaskDone) {
      if (store.todos.length !== 0) {
        store.todos.map((e, index) => {
          if (e.done) {
            setValueTaskDone((prevState) => [
              ...prevState,
              { task: e.name, done: e.done },
            ]);
            store.removeTodoDone(e.name);
            setHideShowTaskDone(false);
          }
        });
      }
    } else {
      valueTaskDone?.map((e) => {
        store.addTodo(getSnapshot(store).todos.length, e.task, e.done);
        setHideShowTaskDone(true);
        setValueTaskDone([]);
      });
    }
  };

  return (
    <React.Fragment>
      <div css={container}>
        <div css={title}>Lista de Tareas</div>
        <div css={containerTaskDone}>
          <button
            css={buttonTaskDone}
            onClick={() => ShowTaskDone()}
            id="btn-showTask"
          >
            {hideShowTaskDone ? "Ocultar completadas" : "Mostrar completadas"}
          </button>
        </div>
        <div css={containerInputTask}>
          <input
            data-testid="task-input"
            id="newTask"
            type="text"
            value={valueTask.task}
            placeholder="Nueva tarea"
            css={inputTask}
            onChange={handleOnChange}
            onKeyPress={handleKeyPress}
            required
          />
          <button
            data-testid="button-sendTask"
            css={buttonInpuTask}
            onClick={(e) => sendTask(e)}
            disabled={checkSendTask}
          >
            Agregar
          </button>
        </div>
        <ListTask
          handleCheck={handleCheck}
          valueTaskDone={valueTaskDone}
          hideShowTaskDone={hideShowTaskDone}
        />
      </div>
    </React.Fragment>
  );
};

export default observer(TodoTask);
