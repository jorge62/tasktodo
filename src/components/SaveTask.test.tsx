import React, { ChangeEvent } from "react";
import "@testing-library/jest-dom/extend-expect";
import { fireEvent, getByTestId, render, screen } from "@testing-library/react";
import SaveTask from "./SaveTask";
import userEvent from "@testing-library/user-event";
import TodoTask from "./TodoTask";

describe("<SaveTask />", () => {
  test("renders content", () => {
    console.log(
      render(
        <SaveTask
          handleChangeTask={function (
            event: ChangeEvent<HTMLInputElement>
          ): void {
            throw new Error("Function not implemented.");
          }}
          handleSaveTask={function (
            event: ChangeEvent<HTMLInputElement>
          ): void {
            throw new Error("Function not implemented.");
          }}
          handleKeyPress={function (
            event: React.KeyboardEvent<HTMLInputElement>
          ): void {
            throw new Error("Function not implemented.");
          }}
        />
      )
    );
  });

  test("Exist text Save ?", () => {
    render(
      <SaveTask
        handleChangeTask={function (
          event: ChangeEvent<HTMLInputElement>
        ): void {
          throw new Error("Function not implemented.");
        }}
        handleSaveTask={function (event: ChangeEvent<HTMLInputElement>): void {
          throw new Error("Function not implemented.");
        }}
        handleKeyPress={function (
          event: React.KeyboardEvent<HTMLInputElement>
        ): void {
          throw new Error("Function not implemented.");
        }}
      />
    );
    screen.getByText("Guardar");
  });

  test("button should be exist in screen", async () => {
    render(
      <SaveTask
        handleChangeTask={function (
          event: ChangeEvent<HTMLInputElement>
        ): void {
          throw new Error("Function not implemented.");
        }}
        handleSaveTask={function (event: ChangeEvent<HTMLInputElement>): void {
          throw new Error("Function not implemented.");
        }}
        handleKeyPress={function (
          event: React.KeyboardEvent<HTMLInputElement>
        ): void {
          throw new Error("Function not implemented.");
        }}
      />
    );
    const items = await screen.findAllByRole("button");
    expect(items).toHaveLength(1);
  });

  test("render input Task", () => {
    render(
      <SaveTask
        handleChangeTask={function (
          event: ChangeEvent<HTMLInputElement>
        ): void {
          throw new Error("Function not implemented.");
        }}
        handleSaveTask={function (event: ChangeEvent<HTMLInputElement>): void {
          throw new Error("Function not implemented.");
        }}
        handleKeyPress={function (
          event: React.KeyboardEvent<HTMLInputElement>
        ): void {
          throw new Error("Function not implemented.");
        }}
      />
    );
    const inputT = screen.getByTestId("save-input");
    expect(inputT).toBeInTheDocument();
    expect(inputT).toHaveAttribute("type", "text");
  });
});
