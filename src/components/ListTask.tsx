import React from "react";
/** @jsxRuntime classic */
/** @jsx jsx */
import { jsx, css } from "@emotion/react";
import { store } from "../store/TodoStore";
import { getSnapshot } from "mobx-state-tree";
import { observer } from "mobx-react";
import SaveTask from "./SaveTask";
import { HandleCheck } from "../interface/HandleCheck";
import Swal from "sweetalert2";
import UseSaveTask from "./hooks/UseSaveTask";

const boxTasks = css`
  display: flex;
  flex-direction: column;
  justify-content: space-around;
  margin-left: auto;
  margin-right: auto;
  margin-top: 0%;
  width: 700px;
  height: auto;
  background: #ffffff;
  border: 1px solid #bebebe;
  border-radius: 10px;
  text-align: left;
  margin-bottom: 2rem;
  box-shadow: rgba(50, 50, 93, 0.25) 0px 13px 27px -5px,
    rgba(0, 0, 0, 0.3) 0px 8px 16px -8px;
`;

const boxCheckTask = css`
  margin-top: 1rem;
  margin-left: 1rem;
  font-size: 20px;
`;

const boxCheckTaskLabel = css`
  margin-left: 0.8rem;
  cursor: pointer;
`;

const checkedItem = css`
  margin-left: 0.8rem;
  text-decoration: line-through;
`;

const ListTask: React.FC<HandleCheck> = (props: HandleCheck) => {
  const [showEdiTask, setShowEditTask] = React.useState(false);

  const [valueTask, setValueTask] = React.useState<{
    task: string;
  }>({ task: "" });

  const [taskToSave, setTaskToSave] = React.useState<{
    task: string;
  }>({ task: "" });

  React.useEffect(() => {
    if (props.hideShowTaskDone) {
      try {
        const save = JSON.parse(localStorage.getItem("TasksHidens") || "");
        save?.map((e: any) => {
          store.addTodo(getSnapshot(store).todos.length, e.task, e.done);
        });
      } catch (error) {
        console.log("Error: ", error);
      }
    }
  }, []);

  React.useEffect(() => {
    setShowEditTask(true);
  }, [valueTask]);

  const handleLabel = (e: any) => {
    const id = e.target.id;
    setValueTask((prevState) => ({ ...prevState, task: id }));
  };

  const handleChangeTask = (e: any) => {
    const task = e.target.value;
    setTaskToSave((prevState) => ({ ...prevState, task }));
  };

  const handleSaveTask = (event: any) => {
    UseSaveTask(
      event,
      taskToSave,
      valueTask,
      setShowEditTask,
      setValueTask,
      setTaskToSave
    );
  };

  const handleKeyPress = (event: React.KeyboardEvent<HTMLDivElement>) => {
    UseSaveTask(
      event,
      taskToSave,
      valueTask,
      setShowEditTask,
      setValueTask,
      setTaskToSave
    );
  };

  return (
    <React.Fragment>
      <div css={boxTasks}>
        {getSnapshot(store).todos.length !== 0
          ? getSnapshot(store).todos.map((todo, index) => {
              return (
                <React.Fragment key={index}>
                  <div css={boxCheckTask}>
                    <input
                      data-testid="checkTaskDone"
                      type="checkbox"
                      name={todo.name}
                      id={index.toString()}
                      onChange={(e) => props.handleCheck(e)}
                      value={todo.name}
                      checked={todo.done}
                    />
                    <label
                      data-testid="labelTask"
                      id={todo.name}
                      css={todo.done ? checkedItem : boxCheckTaskLabel}
                      onClick={(e) => handleLabel(e)}
                    >
                      {todo.name}
                    </label>
                  </div>
                  {todo.name === valueTask.task && showEdiTask ? (
                    <SaveTask
                      handleChangeTask={handleChangeTask}
                      handleSaveTask={handleSaveTask}
                      handleKeyPress={handleKeyPress}
                    />
                  ) : (
                    ""
                  )}
                  <br></br>
                </React.Fragment>
              );
            })
          : ""}
      </div>
    </React.Fragment>
  );
};

export default observer(ListTask);
