import React, { ChangeEvent } from "react";
import "@testing-library/jest-dom/extend-expect";
import { fireEvent, getByTestId, render, screen } from "@testing-library/react";
import ListTask from "./ListTask";
import userEvent from "@testing-library/user-event";

describe("<ListTask />", () => {
  test("renders content", () => {
    console.log(
      render(
        <ListTask
          handleCheck={function (event: ChangeEvent<HTMLInputElement>): void {
            throw new Error("Function not implemented.");
          }}
          valueTaskDone={[]}
          hideShowTaskDone={false}
        />
      )
    );
  });

  test("changes style, checkbox is checked/unchecked", () => {
    const { container } = render(
      <ListTask
        handleCheck={function (event: ChangeEvent<HTMLInputElement>): void {
          throw new Error("Function not implemented.");
        }}
        valueTaskDone={[]}
        hideShowTaskDone={false}
      />
    );
    // const checkbox: HTMLElement = screen.queryByTestId(
    //   "checkTaskDone"
    // ) as HTMLElement;
    // expect(checkbox).toHaveAttribute("type", "checkbox");
    // const checkbox: HTMLInputElement = screen.getByTestId("checkTaskDone");

    // expect(checkbox.checked).toEqual(false);
    // expect(label.style["textDecoration"]).toEqual("none");
    // fireEvent.click(checkbox);
    // expect(checkbox.checked).toEqual(true);
    // expect(label.style["textDecoration"]).toEqual("line-through");
    // fireEvent.click(checkbox);
    // expect(checkbox.checked).toEqual(false);
    // expect(label.style["textDecoration"]).toEqual("none");
  });
});
