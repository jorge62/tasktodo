import React, { useEffect } from "react";
import Swal from "sweetalert2";
import { store } from "../../store/TodoStore";

export default function UseAddTask(
  e: any,
  valueTask: any,
  indexStorage: number,
  setValueTask: any
) {
  if (e.key === "Enter" || e.type === "click") {
    e.preventDefault();
    try {
      const save = JSON.parse(localStorage.getItem("TasksHidens") || "");
      const sameName = save.filter((e: any) => e.task === valueTask.task);
      if (sameName.length === 0) {
        store
          .addTodo(indexStorage, valueTask.task, valueTask.done)
          .then((res) => {
            if (res === 1) {
              Swal.fire({
                icon: "error",
                title: "Erorr",
                text: "Ya existe la tarea",
              });
            }
          });
        setValueTask((prevState: any) => ({
          ...prevState,
          task: "",
        }));
      } else {
        Swal.fire({
          icon: "error",
          title: "Erorr",
          text: "Ya existe la tarea",
        });
      }
    } catch (error) {
      console.log("Error: ", error);
    }
    setValueTask((prevState: any) => ({
      ...prevState,
      task: "",
    }));
  }
}
