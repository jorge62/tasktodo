import React from "react";
import Swal from "sweetalert2";
import { store } from "../../store/TodoStore";

export default function UseSaveTask(
  e: any,
  taskToSave: any,
  valueTask: any,
  setShowEditTask: any,
  setValueTask: any,
  setTaskToSave: any
) {
  if (e.key === "Enter" || e.type === "click") {
    if (taskToSave.task.length !== 0) {
      try {
        const save = JSON.parse(localStorage.getItem("TasksHidens") || "");

        const sameNameSave = save.filter(
          (e: any) => e.task === taskToSave.task
        );

        const sameName = save.filter((e: any) => e.task === valueTask.task);

        if (sameName.length === 0 && sameNameSave.length === 0) {
          store.todos.map((e) => {
            if (e.name === valueTask.task) {
              e.setName(taskToSave.task).then((res) => {
                if (res === 1) {
                  Swal.fire({
                    icon: "error",
                    title: "Erorr",
                    text: "Ya existe la tarea",
                  });
                  setShowEditTask(true);
                } else {
                  setValueTask((prevState: any) => ({
                    ...prevState,
                    task: "",
                  }));
                }
              });
              setShowEditTask(false);
            }
          });
        } else {
          Swal.fire({
            icon: "error",
            title: "Erorr",
            text: "Ya existe la tarea",
          });
          setValueTask((prevState: any) => ({
            ...prevState,
            task: "",
          }));
        }
      } catch (error) {
        console.log("Error: ", error);
      }
    } else {
      setShowEditTask(false);
    }
    setTaskToSave({ task: "" });
  }
}
