import { observer } from "mobx-react";
import React from "react";
/** @jsxRuntime classic */
/** @jsx jsx */
import { jsx, css } from "@emotion/react";
import { ChangeTask } from "../interface/ChangeTask";

const inputTask = css`
  width: 350px;
  height: 40px;
  font-size: 20px;
  border: 1px solid #bebebe;
  border-radius: 8px;
`;

const buttonInputSaveTask = css`
  width: 200px;
  height: 45px;
  font-size: 20px;
  border: 1px solid #bebebe;
  border-radius: 8px;
  margin-left: 1.5rem;
  font-weight: lighter;
  margin-bottom: 2rem;
  cursor: pointer;
`;

const containerSaveTask = css`
  margin-top: 0.5rem;
  margin-bottom: -2.7rem;
  margin-left: 4rem;
`;

const SaveTask: React.FC<ChangeTask> = (props: ChangeTask) => {
  return (
    <React.Fragment>
      <div css={containerSaveTask}>
        <input
          type="text"
          data-testid="save-input"
          css={inputTask}
          onChange={(e) => props.handleChangeTask(e)}
          onKeyPress={(e) => props.handleKeyPress(e)}
        ></input>
        <button
          css={buttonInputSaveTask}
          onClick={(e: any) => props.handleSaveTask(e)}
        >
          Guardar
        </button>
      </div>
    </React.Fragment>
  );
};

export default observer(SaveTask);
