import React from "react";
import "@testing-library/jest-dom/extend-expect";
import { fireEvent, getByTestId, render, screen } from "@testing-library/react";
import TodoTask from "./TodoTask";
import userEvent from "@testing-library/user-event";

describe("<TodoTask />", () => {
  test("renders content", () => {
    console.log(render(<TodoTask />));
  });

  test("render text placeholder", () => {
    render(<TodoTask />);
    screen.getByPlaceholderText("Nueva tarea");
  });

  test("render buttons", async () => {
    render(<TodoTask />);
    const items = await screen.findAllByRole("button");
    expect(items).toHaveLength(2);
  });

  test("render input Task", () => {
    render(<TodoTask />);
    const inputT = screen.getByTestId("task-input");
    expect(inputT).toBeInTheDocument();
    expect(inputT).toHaveAttribute("type", "text");
  });

  test("when button is pressed should call the 'sendTask' callback", () => {
    render(<TodoTask />);
    const inputT = screen.getByTestId("task-input");
    const button = screen.getByTestId("button-sendTask");
    fireEvent.change(inputT, { target: { value: "123" } });
    fireEvent.click(button);
    expect(screen.getByText("123")).toBeInTheDocument();
  });

  // test("when button is pressed should call the 'ShowTaskDone' callback", () => {
  //   render(<TodoTask />);
  //   const button = screen.getAllByRole("button");
  //   fireEvent.click(button[0]);
  //   expect(screen.getByText("Mostrar completadas")).toBeInTheDocument();
  // });
});
