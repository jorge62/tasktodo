export interface HandleCheck {
  handleCheck: (event: React.ChangeEvent<HTMLInputElement>) => void;
  valueTaskDone: {}[];
  hideShowTaskDone: boolean;
}
