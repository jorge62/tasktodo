export interface ChangeTask {
  handleChangeTask: (event: React.ChangeEvent<HTMLInputElement>) => void;
  handleSaveTask: (event: React.ChangeEvent<HTMLInputElement>) => void;
  handleKeyPress: (event: React.KeyboardEvent<HTMLInputElement>) => void;
}
