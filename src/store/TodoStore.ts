import { types, getSnapshot, destroy, detach } from "mobx-state-tree";
import { persist } from "mst-persist";

const listTodo = types
  .model("listTodo", {
    name: types.optional(types.string, ""),
    done: types.optional(types.boolean, false),
  })
  .actions((self) => {
    function setName(newName: string) {
      return new Promise((resolve) => {
        const sameName = store.todos.filter((todo) => todo.name === newName);
        if (sameName.length === 0) {
          if (newName.length !== 0) {
            self.name = newName;
            resolve(newName);
          } else {
            resolve(1);
          }
        } else {
          resolve(1);
        }
      });
    }

    function toggle() {
      self.done = !self.done;
    }

    return { setName, toggle };
  });

const RootStore = types
  .model({
    todos: types.array(listTodo),
  })
  .actions((self) => {
    function addTodo(indexStorage: number, name: string, done: boolean) {
      return new Promise((resolve) => {
        const sameName = self.todos.filter((todo) => todo.name === name);
        if (sameName.length === 0) {
          if (name.length !== 0) {
            self.todos[indexStorage] = listTodo.create({ name, done });
            resolve(name);
          } else {
            resolve(1);
          }
        } else {
          resolve(1);
        }
      });
      // const sameName = self.todos.filter((todo) => todo.name === name);
      // if (sameName.length === 0) {
      //   self.todos[indexStorage] = listTodo.create({ name, done });
      // }
    }
    function removeAllTodo() {
      destroy(self.todos);
    }
    function removeTodoDone(name: string) {
      self.todos.map((todo) => {
        if (todo.name === name) {
          detach(todo);
        }
      });
    }
    return { addTodo, removeTodoDone, removeAllTodo };
  });

export const store = RootStore.create({});

persist("todo", store, {
  storage: localStorage,
  jsonify: true,
  whitelist: ["todos"],
}).then(() => console.log("someStore has been hydrated"));
