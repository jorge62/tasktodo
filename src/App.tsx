import React from "react";
import TodoTask from "./components/TodoTask";

const App: React.FC = () => {
  return <TodoTask />;
};

export default App;
